const widget = new ListWidget();

widget.backgroundColor = new Color("#121212");
widget.setPadding(0, 0, 0, 0);

widget.url = "https://beta.watchcubesburn.art";

const titleStack = widget.addStack();
const authorStack = widget.addStack();

authorStack.setPadding(0, 12, 0, 0);
titleStack.setPadding(12, 12, 0, 18);

titleStack.layoutHorizontally();
authorStack.layoutHorizontally();

const ashBW = await loadImage(
  "https://arweave.net/-lFEnhngMUg-v8gfnY_Y_tV6kiClglnmQ4h7iwHQea8"
);

const headerText = titleStack.addText("Burn");
const coinImm = titleStack.addImage(ashBW);

coinImm.imageSize = new Size(7, 7);
headerText.font = new Font("Helvetica-Bold", 18);

const headerTextnxt = titleStack.addText(" Panel");

headerTextnxt.font = new Font("Helvetica-Bold", 18);
headerText.leftAlignText();

const authorText = authorStack.addText("by secondstate.xyz");
authorText.font = new Font("Helvetica-Bold", 8);
authorText.leftAlignText();

headerText.textColor = new Color("#FFFFFF");
headerTextnxt.textColor = new Color("#FFFFFF");
authorText.textColor = new Color("#FFFFFF");

async function createWidget() {
  const ashImg = await loadImage(
    "https://arweave.net/JwV6d7MGlBAk1knOTPXOHYy1cqxGjJopqiiUj0rrfr8"
  );
  const walletImg = await loadImage(
    "https://arweave.net/OI2uN_U1ArQ2B9Of5x12s92giLdd5bPCRsoBtK7FvRU"
  );
  const fireImg = await loadImage(
    "https://arweave.net/oMACPPuVze4QSQQoaOwIjm1ZXr_CRnwoASeuJuQQkF8"
  );

  const BurnData = await fetchBurnedData();

  const burnrate = BurnData[0];
  const holders = BurnData[1];
  const burnvalue = BurnData[2];

  burnRate(fireImg, `    ${burnrate}`);
  burnValue(ashImg, ` $ ${burnvalue}`);
  holderCount(walletImg, `    ${holders}`);
}

function burnRate(fireimage, burnrate) {
  const burnStack = widget.addStack();
  burnStack.setPadding(0, 6, 6, 0);
  burnStack.layoutHorizontally();

  const brImgStack = burnStack.addStack();
  const brStack = burnStack.addStack();

  brImgStack.setPadding(8, 0, 0, 12);
  brStack.setPadding(12, 0, 6, 18);

  const brImgX = brImgStack.addImage(fireimage);
  brImgX.imageSize = new Size(28, 28);
  brImgX.leftAlignImage();

  const priceText = brStack.addText(burnrate);
  priceText.font = new Font("SF Pro", 18);
  brStack.centerAlignContent();
  priceText.textColor = new Color("#FFFFFF");
}

function burnValue(ashimage, burnvalue) {
  const burnValueStack = widget.addStack();
  burnValueStack.setPadding(0, 6, 6, 0);
  burnValueStack.layoutHorizontally();

  const bvImgStack = burnValueStack.addStack();
  const bvStack = burnValueStack.addStack();

  bvImgStack.setPadding(4, 0, 0, 12);
  bvStack.setPadding(8, 0, 6, 18);

  const bvImgX = bvImgStack.addImage(ashimage);
  bvImgX.imageSize = new Size(28, 28);
  bvImgX.leftAlignImage();

  const priceText = bvStack.addText(burnvalue);
  priceText.font = new Font("SF Pro", 18);
  bvStack.centerAlignContent();
  priceText.textColor = new Color("#FFFFFF");
}

function holderCount(walletimage, holders) {
  const holderStack = widget.addStack();
  holderStack.setPadding(0, 6, 6, 0);
  holderStack.layoutHorizontally();

  const hcImgStack = holderStack.addStack();
  const hcStack = holderStack.addStack();

  hcImgStack.setPadding(4, 0, 6, 12);
  hcStack.setPadding(8, 0, 6, 0);

  const hcImgX = hcImgStack.addImage(walletimage);
  hcImgX.imageSize = new Size(28, 28);
  hcImgX.leftAlignImage();

  const priceText = hcStack.addText(holders);
  priceText.font = new Font("SF Pro", 18);
  priceText.textColor = new Color("#FFFFFF");
}

async function fetchBurnedData() {
  const ethplorer =
    "https://api.ethplorer.io/getTokenInfo/0x64d91f12ece7362f91a6f8e7940cd55f05060b92?apiKey=freekey";
  const burnreq = new Request(ethplorer);
  const burnesp = await burnreq.loadJSON();

  const totalSupplyRound = Math.round(burnesp.totalSupply / 10 ** 18);
  const burnrate = (0.5 ** (totalSupplyRound / 5000000) * 1000).toFixed(2);
  const holders = burnesp.holdersCount
    .toFixed(0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  const burnvalue = (
    (0.5 ** (totalSupplyRound / 5000000) * 1000).toFixed(3) *
    (await (await getUniswapPrice()).at(0))
  )
    .toFixed(0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return [burnrate, holders, burnvalue];
}

async function getUniswapPrice() {
  const uniswap = "https://uniswap.secondstate.workers.dev/";
  const request = new Request(uniswap);
  request.method = "GET";
  request.headers = {
    "Content-Type": "application/json",
  };
  const result = await request.loadJSON();
  const rawprice = result.data.token.derivedETH;
  const ashprice =
    result.data.bundle.ethPriceUSD * result.data.token.derivedETH;
  const ethprice = result.data.bundle.ethPriceUSD;

  return [ashprice, ethprice, rawprice];
}

async function loadImage(imgUrl) {
  const req = new Request(imgUrl);
  return await req.loadImage();
}

await createWidget();

Script.setWidget(widget);
Script.complete();
widget.presentSmall();
