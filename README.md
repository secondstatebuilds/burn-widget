# Burn Widget / Burn Panel for iOS

Burn Widget / Burn Panel for iOS via Scriptable.app
 
 - Burn Widget - Normal:
 <img src="/burn-widget.jpg" alt="Burn Widget for iOS" width="25%" height="25%" />
 - Burn Widget - Extended:
 <img src="/burn-widget-extended.jpg" alt="Burn Widget for iOS" width="25%" height="25%" />
 - Burn Panel:
 <img src="/burn-panel.jpg" alt="Burn Widget / Burn Panel for iOS" width="25%" height="25%" />

## Getting started

- Download the [Scriptable.app](https://apps.apple.com/us/app/scriptable/id1405459188)

- Click the + button in the top right corner and paste the contents of [burnwidget-normal.js](https://gitlab.com/secondstatebuilds/burn-widget/-/raw/main/burnwidget-normal.js), [burnwidget-extended.js](https://gitlab.com/secondstatebuilds/burn-widget/-/raw/main/burnwidget-extended.js) or [burn-panel.js](https://gitlab.com/secondstatebuilds/burn-widget/-/raw/main/burn-panel.js) by manually copying the whole page into the text field, or click here -> [burnwidget-normal.js-mainpage](https://gitlab.com/secondstatebuilds/burn-widget/-/blob/main/burnwidget-normal.js), [burnwidget-extended.js-mainpage](https://gitlab.com/secondstatebuilds/burn-widget/-/blob/main/burnwidget-extended.js) or [burn-panel.js-mainpage](https://gitlab.com/secondstatebuilds/burn-widget/-/blob/main/burn-panel.js) then click the "Copy file contents" button. 
Finally, click "Done" in Scriptable.app.

- Long press your home screen, click the + sign in the upper left corner then search for "Scriptable" and click "Add Widget".

- Click the "Select script in widget configurator" text, then select the script you previously created for the "Script" dropdown. Finally, set "When Interacting" to "Run Script" then click away. You should now see a new widget appear on your home screen. :]


## Details

Burn Widget - Normal: 

- Downtrends are indicated as RED text, while uptrends are indicated as WHITE text.

- Both ASH and ETH rate are pulled from [Uniswap v3](https://info.uniswap.org/#/tokens/0x64d91f12ece7362f91a6f8e7940cd55f05060b92), and clicking the widget also takes you to the charts of Uniswap.

- Trends are pulled from [CoinGecko](https://www.coingecko.com/)

Burn Widget - Extended:

- All the above from Small + raw ETH-ASH price.


Burn Panel:

- High-tier burn rate for Pak NFTs.

- Total ASH holder wallets.

- Value of burned ASH in USD. (Uniswap v3 based - does not account gas / slippage costs.)

## Credits

- [Dough Boi](https://twitter.com/DoughBoiNFT) for the UI/UX design assistance.
- ASH is volatile. Price != Value.
- ?



## TODO

- Submit app to Scriptable.app store. 
- Possible styling?




