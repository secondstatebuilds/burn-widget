const widget = new ListWidget();

widget.backgroundColor = new Color("#121212");
widget.setPadding(0, 0, 0, 0);

widget.url =
  "https://info.uniswap.org/#/tokens/0x64d91f12ece7362f91a6f8e7940cd55f05060b92";

const titleStack = widget.addStack();
const authorStack = widget.addStack();

authorStack.setPadding(0, 12, 0, 0);
titleStack.setPadding(12, 12, 0, 18);

titleStack.layoutHorizontally();
authorStack.layoutHorizontally();

const ashBW = await loadImage(
  "https://arweave.net/-lFEnhngMUg-v8gfnY_Y_tV6kiClglnmQ4h7iwHQea8"
);

const headerText = titleStack.addText("Burn");
const coinImm = titleStack.addImage(ashBW);

coinImm.imageSize = new Size(7, 7);
headerText.font = new Font("Helvetica-Bold", 18);

const headerTextnxt = titleStack.addText(" Widget");

headerTextnxt.font = new Font("Helvetica-Bold", 18);
headerText.leftAlignText();

const authorText = authorStack.addText("by secondstate.xyz");
authorText.font = new Font("Helvetica-Bold", 8);
authorText.leftAlignText();

headerText.textColor = new Color("#FFFFFF");
headerTextnxt.textColor = new Color("#FFFFFF");
authorText.textColor = new Color("#FFFFFF");

async function createWidget() {
  const ashImage = await loadImage(
    "https://arweave.net/JwV6d7MGlBAk1knOTPXOHYy1cqxGjJopqiiUj0rrfr8"
  );
  const ethereumImage = await loadImage(
    "https://arweave.net/YT6sOoJkwwskx6LwZDLApxlAdlcdk5_pDFFQ_rBJJ2k"
  );

  const UniPrices = await getUniswapPrice();
  const trend = await getTrend();

  const rawAshPrice = UniPrices.rawprice.toString().substring(0, 6);
  const AshPrice = UniPrices.ashprice.toFixed(2);
  const EthPrice = parseInt(UniPrices.ethprice, 10)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  addAsh(ashImage, ` $ ${AshPrice}`, trend.ashtrend);
  addEth(ethereumImage, ` $ ${EthPrice}`, trend.ethtrend);
  addRawAsh(ashImage, ` Ξ ${rawAshPrice}`, trend.ashtrend);
}

function addAsh(ashimage, ashprice, ashtrend) {
  const ashStack = widget.addStack();
  ashStack.setPadding(0, 6, 6, 0);
  ashStack.layoutHorizontally();

  const ASHimgStack = ashStack.addStack();
  const ashPriceStack = ashStack.addStack();

  ASHimgStack.setPadding(8, 0, 0, 12);
  ashPriceStack.setPadding(12, 0, 6, 18);

  const ashImgX = ASHimgStack.addImage(ashimage);
  ashImgX.imageSize = new Size(28, 28);
  ashImgX.leftAlignImage();

  const priceText = ashPriceStack.addText(ashprice);
  priceText.font = new Font("SF Pro", 18);
  ashPriceStack.centerAlignContent();

  if (ashtrend) {
    priceText.textColor = new Color("#FFFFFF");
  } else {
    priceText.textColor = new Color("#FA124F");
  }
}

function addEth(ethimage, ethprice, ethtrend) {
  const ethStack = widget.addStack();
  ethStack.setPadding(0, 6, 6, 0);
  ethStack.layoutHorizontally();

  const ETHimgStack = ethStack.addStack();
  const ethPriceStack = ethStack.addStack();

  ETHimgStack.setPadding(4, 0, 0, 12);
  ethPriceStack.setPadding(8, 0, 6, 18);

  const ethImgX = ETHimgStack.addImage(ethimage);
  ethImgX.imageSize = new Size(28, 28);
  ethImgX.leftAlignImage();

  const priceText = ethPriceStack.addText(ethprice);
  priceText.font = new Font("Semi", 18);

  if (ethtrend) {
    priceText.textColor = new Color("#FFFFFF");
  } else {
    priceText.textColor = new Color("#FA124F");
  }
}

function addRawAsh(ashimage, ashprice, ashtrend) {
  const ashStack = widget.addStack();
  ashStack.setPadding(0, 6, 6, 0);
  ashStack.layoutHorizontally();

  const ASHimgStack = ashStack.addStack();
  const ashPriceStack = ashStack.addStack();

  ASHimgStack.setPadding(4, 0, 6, 12);
  ashPriceStack.setPadding(8, 0, 6, 0);

  const ashImgX = ASHimgStack.addImage(ashimage);
  ashImgX.imageSize = new Size(28, 28);
  ashImgX.leftAlignImage();

  const priceText = ashPriceStack.addText(ashprice);
  priceText.font = new Font("Semi", 18);
  ashPriceStack.centerAlignContent();

  if (ashtrend) {
    priceText.textColor = new Color("#FFFFFF");
  } else {
    priceText.textColor = new Color("#FA124F");
  }
}

async function getTrend() {
  const ash =
    "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ash";
  const eth =
    "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ethereum";
  const ashreq = new Request(ash);
  const ethreq = new Request(eth);
  const ashres = await ashreq.loadJSON();
  const ethres = await ethreq.loadJSON();
  const ashtrend = ashres[0].price_change_24h > 0;
  const ethtrend = ethres[0].price_change_24h > 0;
  return { ashtrend, ethtrend };
}

async function getUniswapPrice() {
  const uniswap = "https://uniswap.secondstate.workers.dev/";
  const request = new Request(uniswap);
  request.method = "GET";
  request.headers = {
    "Content-Type": "application/json",
  };
  const result = await request.loadJSON();
  const rawprice = result.data.token.derivedETH;
  const ashprice =
    result.data.bundle.ethPriceUSD * result.data.token.derivedETH;
  const ethprice = result.data.bundle.ethPriceUSD;

  return { ashprice, ethprice, rawprice };
}

async function loadImage(imgUrl) {
  const req = new Request(imgUrl);
  return await req.loadImage();
}

await createWidget();

Script.setWidget(widget);
Script.complete();
widget.presentSmall();
